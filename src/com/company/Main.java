package com.company;

import java.math.BigInteger;

public class Main {
    private static final int minSearch = 401;
    private static final int maxSearch = 800;
    private static final int[] fivePrefixes = new int[] {1, 3}; // correspond to no prefix, 3
    private static final int[] noFivePrefixes = new int[] {1, 2, 12, 3, 4, 6}; // correspond to no prefix, 2, 26, 3, 4, 6

    private static long currentHighSteps = 0;
    private static BigInteger currentResult = BigInteger.ZERO;

    public static void main(String[] args) {
        intelligentSearch();
    }

    private static void intelligentSearch() {
        BigInteger[] arr5, arr7, arr8, arr9;

        // Contains all powers of 5, 7, 8, or 9 from 0 to maxSearch (thus taking maxSearch + 1 space)
        arr5 = new BigInteger[maxSearch + 1];
        arr7 = new BigInteger[maxSearch + 1];
        arr8 = new BigInteger[maxSearch + 1];
        arr9 = new BigInteger[maxSearch + 1];

        // Populate arrays of powers
        arr5[0] = arr7[0] = arr8[0] = arr9[0] = BigInteger.ONE;
        for (int i=1; i < arr5.length; i++) {
            arr5[i] = arr7[i-1].multiply(BigInteger.valueOf(5));
            arr7[i] = arr7[i-1].multiply(BigInteger.valueOf(7));
            arr8[i] = arr8[i-1].multiply(BigInteger.valueOf(8));
            arr9[i] = arr9[i-1].multiply(BigInteger.valueOf(9));
        }

        int sevens, fivesOrEights, nines, persistence;
        for (int searchLen = minSearch; searchLen < maxSearch + 1; searchLen++) {
            System.out.println("Searching length " + searchLen + ".");

            for (nines = 0; nines < searchLen + 1; nines++) {
                for (sevens = 0; sevens < searchLen + 1 - nines; sevens++) {
                    fivesOrEights = searchLen - sevens - nines;

                    // Check numbers which contain 5s
                    for (int prefix : fivePrefixes) {
                        persistence = persistence(BigInteger.valueOf(prefix).multiply(arr5[fivesOrEights]).multiply(arr7[sevens]).multiply(arr9[nines])) + 1;
                        if (persistence > currentHighSteps) {
                            currentHighSteps = persistence;
                            currentResult = buildNumber(prefix, fivesOrEights, sevens, 0, nines);
                        } else if (persistence == currentHighSteps) {
                            BigInteger result = buildNumber(prefix, fivesOrEights, sevens, 0, nines);

                            if (result.compareTo(currentResult) == -1) { // if new result is less than current result
                                currentHighSteps = persistence;
                                currentResult = result;

                                System.out.println("New high score of " + persistence + " from " + currentResult + ".");
                            }
                        }
                    }

                    // Check numbers without 5s
                    for (int prefix : noFivePrefixes) {
                        persistence = persistence(BigInteger.valueOf(prefix).multiply(arr8[fivesOrEights]).multiply(arr7[sevens]).multiply(arr9[nines])) + 1;
                        if (persistence > currentHighSteps) {
                            currentHighSteps = persistence;
                            currentResult = buildNumber(prefix, fivesOrEights, sevens, 0, nines);
                        } else if (persistence == currentHighSteps) {
                            BigInteger result = buildNumber(prefix, fivesOrEights, sevens, 0, nines);

                            if (result.compareTo(currentResult) == -1) { // if new result is less than current result
                                currentHighSteps = persistence;
                                currentResult = result;

                                System.out.println("New high score of " + persistence + " from " + currentResult + ".");
                            }
                        }
                    }
                }
            }
        }
    }

    private static int persistence(BigInteger input) {
        int persistence = 0;
        BigInteger digit, nextInt;

        while (input.compareTo(BigInteger.valueOf(9)) == 1) { // while input > 9
            persistence++;
            nextInt = new BigInteger("1");

            do {
                digit = input.mod(BigInteger.valueOf(10));
                input = input.divide(BigInteger.valueOf(10));
                nextInt = nextInt.multiply(digit);
            } while (input.compareTo(BigInteger.valueOf(0)) == 1); // while input > 0

            input = nextInt;
        }

        return persistence;
    }

    private static BigInteger buildNumber(int prefix, int fives, int sevens, int eights, int nines) {

        String str;

        switch (prefix) {
            case 1:
                str = "0";
                break;
            case 2:
                str = "2";
                break;
            case 12:
                str = "26";
                break;
            case 3:
                str = "3";
                break;
            case 4:
                str = "4";
                break;
            case 6:
                str = "6";
                break;
            default:
                str = "ERROR";
                break;
        }

        for (int i=0; i<fives; i++) str += "5";
        for (int i=0; i<sevens; i++) str += "7";
        for (int i=0; i<eights; i++) str += "8";
        for (int i=0; i<nines; i++) str += "9";

        return new BigInteger(str);
    }
}
